import os
import sys
import pefile
import operator

map_funcs = {}

def process_file(file_path):
    global map_funcs
    try:
        pe = pefile.PE(file_path)
        pe.parse_data_directories()
        for entry in pe.DIRECTORY_ENTRY_IMPORT:
            print entry.dll
            for imp in entry.imports:
                print '\t', hex(imp.address), imp.name
                func_count = 0
                try:
                    func_count = map_funcs[imp.name]
                except:
                    map_funcs[imp.name] = 0
                map_funcs[imp.name] = func_count + 1
    except:
        print '[ERR] ', file_path, ' is not a PE file'

def main():
    global map_funcs
    my_dir = "D:/_MasterWork/txt2bmp-master/bin"
    if (my_dir[-1] != '/'):
        my_dir += '/'
    print ' *** Process PE files at: ', my_dir

    for root, dirs, files in os.walk(my_dir):
        for filename in files:
            process_file(my_dir + filename)

    map_funcs_sorted = sorted(map_funcs.items(), key=operator.itemgetter(1), reverse=True)
    for key, value in map_funcs_sorted.iteritems():
        print '%s:%d' % (key, value)

if __name__ == '__main__':
    main()
