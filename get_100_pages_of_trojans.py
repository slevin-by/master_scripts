import sys
import requests
from bs4 import BeautifulSoup

# 1) https://avcaesar.malware.lu/
# 2) https://avcaesar.malware.lu/sample/search?query=trojan&page=1000
# 3) https://avcaesar.malware.lu/sample/7035fbec7b766834ef19e1e648cc60c6a87751d58daf494efa8abee8c7a6a8ed

main_domain = 'https://avcaesar.malware.lu'

map_FuncIndex = {}
func_count = 0
input_pattern = ""
fd_inputs = None

def init_index_map():
    global map_FuncIndex
    global func_count
    global input_pattern
    with open('func_names.txt') as fp:
        line = fp.readline()
        while line:
            map_FuncIndex[line.strip().lower()] = func_count
            line = fp.readline()
            func_count += 1
    for i in range(0, func_count):
        input_pattern += '0'

def is_positive_function(func_name):
    for func_key, index in map_FuncIndex.iteritems():
        if func_name.find(func_key) == 0:
            return func_key
    return "-1"

def process_func_list(func_list):
    global map_FuncIndex
    global func_count
    global input_pattern
    global fd_inputs
    sz_inputs = list(input_pattern)
    for func in func_list:
        #if func.lower() in map_FuncIndex:
        func_key = is_positive_function(func.lower())
        if func_key != "-1":
            #index = map_FuncIndex[func.lower()]
            index = map_FuncIndex[func_key]
            sz_inputs[index] = '1'
    fd_inputs.write(''.join(sz_inputs))
    fd_inputs.write('\n')

def save_page():
    #url = '%s/search?query=caphaw' % (main_domain)
    url = 'https://avcaesar.malware.lu/sample/7035fbec7b766834ef19e1e648cc60c6a87751d58daf494efa8abee8c7a6a8ed'
    r = requests.get(url)
    with open('page_result.html', 'w') as output_file:
        output_file.write(r.text)

def get_links(name, page):
    page = requests.get('%s/sample/search?query=%s&page=%d' % (main_domain, name, page))
    parsed_html = BeautifulSoup(page.content, "html.parser")
    page_links = []
    hashes = parsed_html.findAll("a", {"class": "hash"})
    for link in hashes:
        page_links.append(link.get('href'))
    return page_links

def get_functions(link):
    page = requests.get('%s/%s' % (main_domain, link))
    parsed_html = BeautifulSoup(page.content, "html.parser")
    func_list = []
    funcs = parsed_html.findAll("span", {"class": "label label-iat"})
    for func in funcs:
        #print (func.contents)[0].encode('ascii', 'ignore')
        func_list.append((func.contents)[0].encode('ascii', 'ignore'))
    return func_list

def main():
    global fd_inputs
    name = 'trojan'

    init_index_map()
    fd_inputs = open('inputs.txt', 'w')

    for page in range(1, 1000+1):
        links = get_links(name, page)
        for link in links:
            funcs = get_functions(link)
            if (len(funcs) == 0):
                continue
            process_func_list(funcs)

    fd_inputs.close()

if __name__ == '__main__':
    #save_page()
    main()

