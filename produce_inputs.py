import os
import sys
import pefile

map_FuncIndex = {}
func_count = 0
input_pattern = ""
fd_inputs = None

def init_index_map():
    global map_FuncIndex
    global func_count
    global input_pattern
    with open('func_names.txt') as fp:
        line = fp.readline()
        while line:
            map_FuncIndex[line.strip().lower()] = func_count
            line = fp.readline()
            func_count += 1
    for i in range(0, func_count):
        input_pattern += '0'

def process_file(file_path):
    global map_FuncIndex
    global func_count
    global input_pattern
    try:
        sz_inputs = list(input_pattern)
        pe = pefile.PE(file_path)
        pe.parse_data_directories()
        for entry in pe.DIRECTORY_ENTRY_IMPORT:
            for imp in entry.imports:
                if imp.name in map_FuncIndex:
                    index = map_FuncIndex[imp.name.lower()]
                    sz_inputs[index] = '1'
        fd_inputs.write(''.join(sz_inputs))
        fd_inputs.write('\n')
    except:
        print '[ERR] ', file_path, ' is not a PE file'

def main():
    global fd_inputs

    if len(sys.argv) != 2:
        print 'USAGE: python produce_inputs.py <path_to_dir>'
        sys.exit(1)

    path = sys.argv[1]
    if (path[-1] != '/'):
        path += '/'
    print ' *** Process PE files at: ', path

    fd_inputs = open('inputs.txt', 'w')

    for root, dirs, files in os.walk(path):
        for file_name in files:
            process_file(path + file_name)

    fd_inputs.close()

if __name__ == '__main__':
    init_index_map()
    print func_count
    print map_FuncIndex
    print input_pattern
    #main()
